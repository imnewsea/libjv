(function (jv) {
    if (jv.inBrowser) return;

    const fs = require('fs');
    var path = require('path');
    const child_process = require('child_process');

    var copyFolder2 = function (srcDir, tarPath) {
        var files = fs.readdirSync(srcDir);

        fs.mkdirSync(tarPath, {recursive: true})

        files.forEach(function (file) {
            var srcFile = path.join(srcDir, file);
            var tarFile = path.join(tarPath, file);

            var stats = fs.statSync(srcFile)

            if (stats.isDirectory()) {
                fs.copyFolder(srcFile, tarFile);
            } else {
                fs.copyFileSync(srcFile, tarFile);
            }
        });
    };

    /**
     *
     * @param srcDir 源目录
     * @param targetDir 目标地址
     */
    fs.copyFolder = function (srcDir, targetDir) {
        copyFolder2(srcDir, path.resolve(targetDir, path.basename(srcDir)));
    };

    /**
     * 同步等待
     * @param breakConfCallback
     *
     * 提前删除标志文件：
     * if (fs.statSync(".external.copy.lock", {throwIfNoEntry: false})) {
     *    fs.unlinkSync(".external.copy.lock")
     * }
     *
     * jv.awaitSync( ()=> fs.statSync(".external.copy.lock", {throwIfNoEntry: false}) )
     */
    jv.awaitSync = function (breakConfCallback) {
        var seconds = 1;
        var cmd = "";
        if (process.platform == 'win32') {
            cmd = "timeout /t " + seconds + " /nobreak > nul"
        } else {
            cmd = "sleep " + seconds
        }

        while (true) {
            //fs.statSync(".external.copy.lock", {throwIfNoEntry: false})
            if (breakConfCallback()) {
                break;
            }

            child_process.execSync(cmd, {windowsHide: true, stdio: 'inherit'})
        }
    }

})(jv);
