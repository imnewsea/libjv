module.exports = function (compilation, callback) {
    var path = require("path");
    var fs = require("fs");
    var jv = require("libjv")


    /**
     * 流程：
     * 1. Portal 加载 第三方应用token.html , postMessage  {token} to token.html , --> postMessage {event: token_ack } back.
     * 2. Portal 收到 {event: token_ack} , 加载 第三方应用index.html
     * 3. 第三方应用加载首页！ 可以使用
     */

    /**
     * portal 端：
     jv.eventListener.token_ack = e => {
        this.tokenSaved = true;
     }

     this.$refs['tokenFrame'].postMessage({token: token}, this.url);
     */

    /**
     * 业务系统端：
     var token = localStorage.getItem("token")
     */
    var hi_content = fs.readFileSync(path.join(__dirname, "../res/token.html"), 'utf-8')
        .replaceAll("@VUE_APP_USER_SYSTEM@", process.env.VUE_APP_USER_SYSTEM || "")

    // 将这个列表作为一个新的文件资源，插入到 webpack 构建中：
    compilation.assets['token.html'] = {
        source: function () {
            return hi_content;
        },
        size: function () {
            return hi_content.length;
        }
    };
    callback();
}
