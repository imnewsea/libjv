var sleep = async (duration) => {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, duration);
    });
};

setTimeout(() => {
    (async () => {
        await sleep(2000);//阻塞该async方法的执行线程，直到sleep()返回的Promise resolve
        console.log("after sleep test!");
    })();
}, 0);
