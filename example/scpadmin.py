#!/usr/bin/env python3
#coding=utf-8

import os, sys, getopt


server_ip="dev8.cn"
server_path="/opt/site/admin-web"
shell_name=""
app_jar_name="dist/*"
name="site"
process=[]

def setWorkPath():
    base_path =  os.path.abspath( os.path.join( __file__ ,"../" ) )
    os.chdir( base_path)


def p(code):
    if( code >> 8 ):
        print()
        print("系统出现错误!")
        sys.exit(code)


if __name__=='__main__':
    setWorkPath()

    # cmd = '''rsync -av -e ssh --exclude='./dist/tinymce/' %s root@%s:%s/''' % (app_jar_name, server_ip,server_path)
    cmd = '''scp -r %s root@%s:%s/''' % (app_jar_name, server_ip,server_path)
    print(cmd)
    p(os.system(cmd))


    print('''%s 发布完成'''%(name))

