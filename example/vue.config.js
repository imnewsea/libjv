const path = require('path')
var env_nginx_path = (function() {
    var name = process.env.VUE_APP_NGINX_VPATH_NAME || '';
    if (!name.startsWith('/')) {
        name = '/' + name;
    }
    if (!name.endsWith('/')) {
        name = name + '/';
    }
    return name;
})();

module.exports = {
    publicPath: env_nginx_path,
    lintOnSave: false,
    devServer: {
        port: 8082,
        disableHostCheck: true,
    },
    configureWebpack: config => {
        config.devtool = process.env.NODE_ENV == 'development' ? 'cheap-module-eval-source-map' : ''
        config.externals = {
            // vue: 'Vue',
            // 'element-ui': 'ELEMENT',
            // jquery: "jQuery"
        }
        Object.assign(config.resolve, {
            alias: {
                '@': path.resolve('src'),
                '~': path.resolve('public'),
            }
        })


        config.performance = {
            hints: 'warning',
            //入口起点的最大体积 整数类型（以字节为单位）
            maxEntrypointSize: 50000000,
            //生成文件的最大体积 整数类型（以字节为单位 300k）
            maxAssetSize: 30000000,
            //只给出 js 文件的性能提示
            assetFilter: function (assetFilename) {
                return assetFilename.endsWith('.js')
            }
        };



        // config.module.rules.push(...[
        //     {
        //         test: /\.(vue)$/,
        //         use: [{
        //             loader: 'libjv-loader'
        //         }]
        //     }
        // ])
        // const libjvPlugin = require("libjv-plugin")
        // config.plugins.push(new libjvPlugin())
    },
    chainWebpack:config=>{
        //或者用这种方式,或者用上面 configureWebpack 的方式，二选一进行配置。

        const libjvPlugin = require("libjv-plugin")
        config.plugin("libjv-plugin").use(libjvPlugin)

        config.module.rule("vue").use("libjv-loader").loader("libjv-loader")
    }
}
