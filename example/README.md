# 部署


```
server{
  listen 8401;
  #server_name dev8.cn;
  root /opt/site/admin-web/;
  index index.html;
  location /{
    try_files $uri $uri/ /index.html =404;
  }
}
```

# 工作流组件

## 前端使用npm组件：

> https://www.npmjs.com/package/workflow-bpmn-component

安装：
``` 
yarn add workflow-bpmn-modeler
```

## 实例
1. 工作流列表： /demo/activiti/list
2. 工作流实例列表： /demo/activiti-task/list
3. 修改工作流页面： /demo/activiti/edit/{id}


## 后台微服务：
工作流微服务访问地址： /api/activiti , 其中 /api 是网关， /activiti 是转发到工作流微服务的二级址级。

## 说明


# 多页签

现在的问题是在 首页点击链接,依然会清空.
## 实现思路
> https://router.vuejs.org/zh/guide/advanced/navigation-guards.html

    导航被触发。
    在失活的组件里调用 beforeRouteLeave 守卫。
    调用全局的 beforeEach 守卫。
    在重用的组件里调用 beforeRouteUpdate 守卫 (2.2+)。
    在路由配置里调用 beforeEnter。
    解析异步路由组件。
    在被激活的组件里调用 beforeRouteEnter。
    调用全局的 beforeResolve 守卫 (2.5+)。
    导航被确认。
    调用全局的 afterEach 钩子。
    触发 DOM 更新。
    调用 beforeRouteEnter 守卫中传给 next 的回调函数，创建好的组件实例会作为回调函数的参数传入。


---

    第一次打开：init 流程：watch "$route.path" 获取当前 $route.path , 存到 localStorage, 设置到 iframe

---


    iframe里跳转：监听 iframe.$route.path, 设置到 top.history, 存到 localStorage, 用于显示。

---

## 实现过程
1. 需要 /component/tab-page.vue , /component/tool-bar.vue , App.vue,component.vue,container.vue ,main.js
2. 需要在 路由 上定义 meta:{tab:"页签名称"}
3. 页面中使用 <tool-bar nav="页签定义">右侧按钮区</tool-bar>

    页签定义可以使用字符串,也可以使用数组.
    字符串:  nav="页签名称:路径,页签名称:路径"
    数组:  :nav="[{name:'首页',path:'/'},...]
   
4. nav 默认使用两级，最多显示一级。即：第一级是第一次打开该页签的页面，第二级是当前页面，不显示。 如：
    
    菜单中打开 用户，路径只有一级，面包屑不显示。点 编辑用户，面包屑显示 "用户"
