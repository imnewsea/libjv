import 'babel-polyfill';

import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import jv from './api/enums.js'
import axios from 'axios'

import ElementUI from 'element-ui'
import ElementUIExt from 'element-ui-ext'

import store from './store'

import toolBar from "./layout/tool-bar"

import "./settings"
import permission from "./permission"


(function (jv) {
    window.jv = jv;
    jv.initVue({vue: Vue, axios: axios, router: router, elementUI: ElementUI});
    Vue.config.productionTip = true;

    permission(jv);

    jv.reset = function () {
    };

    jv.debugger = function (v) {
        console.log(v);
    };


    Object.defineProperty(Vue.prototype, "viewer", {
        value() {
            this.$nextTick(() => {
                this.$RecursionVNode(vue => {
                    if (!vue.$vnode) {
                        return;
                    }

                    var tag = vue.$vnode.componentOptions.tag;
                    if (tag) {
                        if (tag == "el-input") {
                            vue.readonly = true;
                            return false;
                        }

                        if (tag == "upload") {
                            vue.readOnly = true;
                            return false;
                        }

                        if (tag == "el-cascader") {
                            vue.disabled = true;
                            return false;
                        }
                    }
                })
            });
        }
    });

    // Vue.prototype.post = Vue.prototype.$http.post;

    // Vue.prototype.post = ()=> {
    //   debugger;
    //   return Vue.prototype.$http.apply(this,arguments);
    // };

    axios.interceptors.request.use(config => {
        var token = jv.token;
        if (token) {
            config.headers.post["token"] = token;
            config.headers.get["token"] = token;
        }

        config.headers["iam"] = "no"
        return config;
    }, error => {
        return Promise.reject(error);
    });


    axios.interceptors.response.use(resp => {
        try {
            var token = resp.headers["token"];
            if (token) {
                jv.token = token;
            }
        } catch (e) {
            console.error("axios.interceptors.response:" + e.message)
            return Promise.reject(e);
        }
        return resp;
    }, error => {
        var resp = error.response;
        if (resp) {
            var token = resp.headers["token"];
            if (token) {
                jv.token = token;
            }
            var status = resp.status;
            if (status == 401) {
                top.jv.main.loginFromVisible = true;
                return Promise.reject(error);
            }
        }
        return Promise.reject(error);
    });

    Vue.use(ElementUI, {size: "small"});
    Vue.use(ElementUIExt);

    Vue.use(() => {
        Vue.component("tool-bar", toolBar)
    })

    jv.size_unit = value => {
        var v = value;
        if (v < 1024) {
            return {value: v.ToRound(), unit: "b"};
        }
        v = v / 1024;
        if (v < 1024) {
            return {value: v.ToRound(), unit: "kb"};
        }
        v = v / 1024;
        if (v < 1024) {
            return {value: v.ToRound(), unit: "mb"};
        }

        v = v / 1024;
        return {value: v.ToRound(), unit: "gb"};
    }

})(jv);


(function (jv) {
    jv.tabs_key = "$tabs"
    var vue = new Vue({
        router,
        store,
        /*        watch: {
                    '$route'(to, from) {
                        jv.loading = false;
                    }
                },*/
        render: h => h(App)
    }).$mount('#app')

    vue.$nextTick(it => {
        // jv.main.loadData();
        document.documentElement.classList.add("loaded");
    });

})(jv);