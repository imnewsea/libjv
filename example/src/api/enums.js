import jv from './jv'


jv.defEnum("UserSexEnum", {Unknown: "未知", Male: "男", Female: "女"});
jv.defEnum("JenkinsLanguageEnum", {Java: "Java", Vue: "Vue", Node: "Node"});
jv.defEnum("JenkinsGitBranchEnum", {dev: "开发版", test: "测试版", demo: "演示版", pre: "预发版", main: "正式版"});
jv.defEnum("DatabaseEnum", {
	Mysql: "Mariadb/Mysql",
	Oracle: "Oracle",
	Sqlite: "Sqlite",
	Mssql: "SqlServer2015+",
	Postgre: "Postgre",

	ElasticSearch: "ElasticSearch",
	Mongo: "Mongo",
	Redis: "Redis",
	Hbase: "Hbase"
});

jv.defEnum("JenkinsJobStatusEnum", {
	None: "找不到",
	Prepare: "准备中",
	Working: "工作中",
	Expired: "已过期",
	Success: "成功",
	Failure: "失败"
});

jv.defEnum("MicroServiceDeployEnvEnum", {"": "保持现状", k8s: "k8s", nacos: "nacos"});

export default jv;


//使用
//jv.OrderStatusEnum.fillRes(jsonRow,"status");
