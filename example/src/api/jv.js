/**
 * Created by udi on 17-4-11.
 */


import jv from "libjv"

jv.admin = {};


jv.admin.getAdminMenus = (data) => {
    // var procDevEnv = list => {
    //     if (window.VUE_APP_ENV_MODE == "dev") return list;
    //     var dev_menus = list.find(it => it.code == "dev")?.menus;
    //
    //     if (!dev_menus) return list;
    //
    //     dev_menus.removeItem(it => it.dependson == "dev")
    //     return list;
    // }
    var url = '/admin/admin-menu/list'
    return jv.ajax.post(url, data).then(res => {
        var list = res.data.data;
        jv.main.menus = list;
        return list;
    });
}

export default jv;
