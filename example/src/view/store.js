import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"


Vue.use(Vuex)

const state = {}
/**
 * mutations 里面放置的是我们操作state对象属性的方法
 */
const mutations = {
}
const actions = {}
const getters = {}
export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters,
    plugins: [createPersistedState({
        reducer(val) {
            return {
                querys: val.querys
            }
        }
    })]
})
