import Vue from 'vue'
import Router from 'vue-router'
import auto from "./auto"
import index from '../view/dev/sys/Index.vue'

Vue.use(Router);

var router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        ...auto,
        {
            path: '/',
            meta: {tab: "首页"},
            component: index
        },
        {
            path:"/vue/immediate",
            component: () => import("../view/dev/vue/immediate")
        },
        {
            path:"/selector/un-match",
            component: () => import("../view/dev/selector/un-match")
        }
    ]
});


//-----------------------------------
// jv.initRouter(router);

export default router;


