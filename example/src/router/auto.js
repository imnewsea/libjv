import "../api/jv";

/**
 * 格式： group: ["实体"]
 * 对应的文件路径：
 * 1. ../view/{group}/{entity}/{entity}-list.vue
 * 1. ../view/{group}/{entity}/{entity}-card.vue
 * 1. ../view/{group}/{entity}/{entity}-ref.vue
 */
var data = {
	admin: {
		  "AdminMenu": "菜单",
	}
};

var routes = [];

Object.keys(data).forEach(_group => {
	var group = _group.getKebabCase();
	var ents_def = data[_group];
	Object.keys(ents_def).forEach(entity => {
		var tabName = ents_def[entity];
		entity = entity.getKebabCase();
		var path_part = `${group}/${entity}/${entity}-`

		routes.push({
			path: `/${group}/${entity}/list`,
			meta: {tab: tabName},
			component: resolve => require([`../view/${path_part}list.vue`], resolve)
		})

		routes.push({
			path: `/${group}/${entity}/add`,
			meta: {tab: tabName},
			component: resolve => require([`../view/${path_part}card.vue`], resolve)
		})

		routes.push({
			path: `/${group}/${entity}/edit/:id`,
			meta: {tab: tabName},
			props: true,
			component: resolve => require([`../view/${path_part}card.vue`], resolve)
		})

		routes.push({
			path: `/${group}/${entity}/ref`,
			component: resolve => require([`../view/${path_part}ref.vue`], resolve)
		})
	});
});

export default routes;
